# node-application chef cookbook

Deploys Node.js applications

Intended for use with AWS OpsWorks for Stacks

## `node-application::setup` recipe

Runs the following recipes:

1. `node-application::add_user` - creates a `node` user to run the Node.js application under.
2. `node-application::install_software` - installs Node.js.
3. `node-application::install_keys` - installs the bitbucket deploy SSH key.
4. `node-application::configure_daemon` - creates the upstart script with all the environment variables needed to run the Node.js application.

## `node-application::deploy` recipe

Runs the following recipes:

1. `node-application::deploy_app` - downloads the Node.js application source code.
2. `node-application::init_app` - executes the npm `init` script and installs the Node.js application dependencies.
3. `node-application::build_app` - executes the npm `build` script.
4. `node-application::start_app` - restarts the Node.js application.
