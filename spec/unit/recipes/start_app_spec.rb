#
# Cookbook:: node-application
# Spec:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'node-application::start_app' do
  context 'When all attributes are default, on an unspecified platform' do
    let(:chef_run) do
      runner().converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end
