require 'chefspec'
require 'chefspec/berkshelf'
require 'json'

def runner
  @data_bags_path = File.expand_path('~/.data_bags')
  @aws_opsworks_app_bag_path = File.join(@data_bags_path, 'aws_opsworks_app/aws_opsworks_app.json')
  @aws_opsworks_app = JSON.parse(IO.read(@aws_opsworks_app_bag_path))

  ChefSpec::ServerRunner.new(platform: 'centos', version: '7.2.1511') do |_, server|
    server.create_data_bag('aws_opsworks_app', 'aws_opsworks_app' => @aws_opsworks_app)
  end
end
