#
# Cookbook:: node-application
# Recipe:: shared_folders
#
# Copyright:: 2017, The Authors, All Rights Reserved.

directory '/opt/apps' do
  mode '755'
  owner 'node'
  group 'node'
  action :create
end

directory '/opt/apps/.ssh' do
  mode '700'
  owner 'node'
  group 'node'
  action :create
end
