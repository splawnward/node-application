#
# Cookbook:: node-application
# Recipe:: deploy
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'node-application::deploy_app'

include_recipe 'node-application::init_app'

include_recipe 'node-application::build_app'

include_recipe 'node-application::start_app'
