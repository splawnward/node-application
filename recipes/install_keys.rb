#
# Cookbook:: node-application
# Recipe:: install_keys
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'node-application::shared_folders'

app = search('aws_opsworks_app').first

file "/opt/apps/.ssh/#{app['shortname']}" do
  mode '0400'
  owner 'node'
  group 'node'
  content app['app_source']['ssh_key']
end

file '/opt/apps/.ssh/config' do
  mode '0600'
  owner 'node'
  group 'node'
  content "Host bitbucket.org\n IdentityFile ~/.ssh/#{app['shortname']}\n IdentitiesOnly yes\n UserKnownHostsFile=/dev/null\n StrictHostKeyChecking no\n"
end
