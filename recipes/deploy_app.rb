#
# Cookbook:: node-application
# Recipe:: install_app
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'node-application::shared_folders'

app = search('aws_opsworks_app').first

git app['shortname'] do
  repository app['app_source']['url']
  revision app['app_source']['revision']
  destination "/opt/apps/#{app['shortname']}"
  environment GIT_SSH_COMMAND: "ssh -i /opt/apps/.ssh/#{app['shortname']} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
  user 'node'
  group 'node'
  action :sync
end
