#
# Cookbook:: node-application
# Recipe:: run_app
#
# Copyright:: 2017, The Authors, All Rights Reserved.

app = search('aws_opsworks_app').first

template "/etc/init/#{app['shortname']}.conf" do
  source 'upstart.conf.erb'
  mode '644'
  variables(
    shortname: app['shortname'],
    environment: app['environment']
  )
  notifies :stop, "service[#{app['shortname']}]", :delayed
  notifies :start, "service[#{app['shortname']}]", :delayed
end

include_recipe 'node-application::start_app'
