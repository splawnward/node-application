#
# Cookbook:: node-application
# Recipe:: start_app
#
# Copyright:: 2017, The Authors, All Rights Reserved.

app = search('aws_opsworks_app').first

service app['shortname'] do
  provider Chef::Provider::Service::Upstart
  action :nothing
  subscribes :restart, "git[#{app['shortname']}]", :delayed
  subscribes :restart, 'execute[npm install]', :delayed
  subscribes :restart, 'execute[npm init]', :delayed
  subscribes :restart, 'execute[npm build]', :delayed
end
