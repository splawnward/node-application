#
# Cookbook:: node-application
# Recipe:: build_app
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'node-application::shared_folders'

app = search('aws_opsworks_app').first

execute 'npm build' do
  cwd "/opt/apps/#{app['shortname']}"
  user 'node'
  environment app['environment'].merge(
    HOME: '/opt/apps',
    USER: 'node'
  )
  command 'npm run build'
end
