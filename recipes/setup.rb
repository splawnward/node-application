#
# Cookbook:: node-application
# Recipe:: setup
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'node-application::add_user'

include_recipe 'node-application::install_software'

include_recipe 'node-application::install_keys'

include_recipe 'node-application::configure_daemon'
