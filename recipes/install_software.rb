#
# Cookbook:: node-application
# Recipe:: install_packages
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'git'

node.default['nodejs']['install_method'] = 'binary'
node.default['nodejs']['version'] = '6.10.1'
node.default['nodejs']['binary']['checksum'] = '31ad1731f4375da2f3ee739f23b0d92c54402eefcc7f98595010395178dde047'

include_recipe 'nodejs'
