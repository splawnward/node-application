#
# Cookbook:: node-application
# Recipe:: add_user
#
# Copyright:: 2017, The Authors, All Rights Reserved.

user 'node' do
  home '/opt/apps'
  system true
  action :create
end

ohai 'reload' do
  action :reload
end

include_recipe 'node-application::shared_folders'
