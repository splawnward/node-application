# Contributing

## Dependencies

Install the required dev tools

* Ruby 1.9 or higher
* Chef 12 DK
* Vagrant
* Vagrant VirtualBox provider

Install the recipe dependencies:

```
$ berks install
```

## Configuration

Create a directory for data bags in your home directory:

```
$ mkdir ~/.data_bags
```

Create the `aws_opsworks_app` data bag:

`~/.data_bags/aws_opsworks_app/aws_opsworks_app.json`:

```json
{
  "id": "aws_opsworks_app",
  "shortname": "my_app",
  "environment": {
    "NODE_ENV": "test",
    "PORT": "8080"
  },
  "app_source": {
    "type": "git",
    "ssh_key": "-----BEGIN RSA PRIVATE KEY-----\n...\n-----END RSA PRIVATE KEY-----",
    "url": "git@bitbucket.org:my/app.git",
    "revision": "master"
  }
}
```

Follow the [AWS App Data Bag format](http://docs.aws.amazon.com/opsworks/latest/userguide/data-bag-json-app.html).

## Test

```
$ foodcritic -B ./
$ delivery local all
```

## Build

```
$ berks install
$ berks package
```

## Publish

Upload the resulting `cookbooks-[timestamp].tar.gz` bundle to `https://s3.amazonaws.com/snw-opsworks-cookbooks/`.

## Reference

* https://docs.chef.io
* http://docs.aws.amazon.com/opsworks/latest/userguide/data-bags.html
* http://kitchen.ci
* https://github.com/chefspec/chefspec
* https://github.com/chefspec/fauxhai
* https://github.com/chef-cookbooks/aws
