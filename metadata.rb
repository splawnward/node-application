name 'node-application'
maintainer 'Splawn & Ward'
maintainer_email 'jhill@splawnandward.com'
license 'all_rights'
description 'Installs/Configures a Node application server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '0.2.0'

%w(centos redhat amazon fedora).each do |os|
  supports os
end

depends 'nodejs'
depends 'git'

source_url 'https://bitbucket.org/splawnward/chef-cookbooks' if respond_to?(:source_url)
issues_url 'https://bitbucket.org/splawnward/node-application/issues' if respond_to?(:issues_url)
chef_version '>= 12.1' if respond_to?(:chef_version)

gem 'json'
